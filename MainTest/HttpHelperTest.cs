﻿using Sorter;
using Sorter.Helpers;
using Sorter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace MainTest
{
    public class HttpHelperTest
    {
        const string serviceEndPoint = @"http://agl-developer-test.azurewebsites.net/people.json";
        [Fact]
        public void TestHttpHelperGet()
        {
            var httpHelper = new HttpHelper();
            var owners = httpHelper.Get<IEnumerable<Owner>>(serviceEndPoint);

            Assert.True(owners != null);
            Assert.True(owners.Any());
            Assert.True(owners.First() is Owner);
            //Assert.True(gameInfo.Id > 0);
        }

        [Fact]
        public void TestHttpHelperInvalidUrl()
        {
            var httpHelper = new HttpHelper();

            var ex = Assert.Throws<Exception>(() => httpHelper.Get<IEnumerable<Owner>>($"{serviceEndPoint}aaaaaaaa"));
            Assert.Contains(CommonResources.ServerError, ex.Message);
        }
    }
}
