﻿using Newtonsoft.Json;
using Sorter;
using Sorter.Helpers;
using Sorter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MainTest
{
    public class IntegrationTest
    {
        const string serviceEndPoint = @"http://agl-developer-test.azurewebsites.net/people.json";

        [Fact]
        public void TestExampleBusinessLogic()
        {
            var httpHelper = new HttpHelper();
            var owners = httpHelper.Get<IEnumerable<Owner>>(serviceEndPoint);

            var sortEngine = new SortEngine();
            var sortedOwnerList = sortEngine.Sort(owners);

            foreach (var gender in sortedOwnerList.Keys)
            {
                var catList = sortedOwnerList[gender].ToList();
                Assert.True(catList.SequenceEqual(catList.OrderBy(s => s))); // Compare the sorted list to the returned list
            }
        }
    }
}