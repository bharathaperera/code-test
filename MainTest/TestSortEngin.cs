using Newtonsoft.Json;
using Sorter;
using Sorter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace MainTest
{
    public class TestSortEngin
    {
        [Fact]
        public void TestExampleBusinessRequirement()
        {
            var sample = "[{\"name\":\"Bob\",\"gender\":\"Male\",\"age\":23,\"pets\":[{\"name\":\"Garfield\",\"type\":\"Cat\"},{\"name\":\"Fido\",\"type\":\"Dog\"}]},{\"name\":\"Jennifer\",\"gender\":\"Female\",\"age\":18,\"pets\":[{\"name\":\"Garfield\",\"type\":\"Cat\"}]},{\"name\":\"Steve\",\"gender\":\"Male\",\"age\":45,\"pets\":null},{\"name\":\"Fred\",\"gender\":\"Male\",\"age\":40,\"pets\":[{\"name\":\"Tom\",\"type\":\"Cat\"},{\"name\":\"Max\",\"type\":\"Cat\"},{\"name\":\"Sam\",\"type\":\"Dog\"},{\"name\":\"Jim\",\"type\":\"Cat\"}]},{\"name\":\"Samantha\",\"gender\":\"Female\",\"age\":40,\"pets\":[{\"name\":\"Tabby\",\"type\":\"Cat\"}]},{\"name\":\"Alice\",\"gender\":\"Female\",\"age\":64,\"pets\":[{\"name\":\"Simba\",\"type\":\"Cat\"},{\"name\":\"Nemo\",\"type\":\"Fish\"}]}]";
            var owners = JsonConvert.DeserializeObject<IEnumerable<Owner>>(sample);
            var expectedCatsOfFemale = new string[] { "Garfield", "Simba", "Tabby" };
            var expectedCatsOfMale = new string[] { "Garfield", "Jim", "Max", "Tom" };

            var sortEngine = new SortEngine();
            var genderList = sortEngine.Sort(owners);

            Assert.Equal(2, genderList.Count);

            var catsOfMale = genderList[Gender.Male]?.ToList();
            for(var i = 0; i < catsOfMale.Count; i++)
            {
                Assert.Equal(expectedCatsOfMale[i], catsOfMale[i]);
            }

            var catsOfFemale = genderList[Gender.Female]?.ToList();
            for (var i = 0; i < catsOfFemale.Count; i++)
            {
                Assert.Equal(expectedCatsOfFemale[i], catsOfFemale[i]);
            }
        }

        [Fact]
        public void TestNoCats()
        {
            var sample = "[{\"name\":\"Bob\",\"gender\":\"Male\",\"age\":23,\"pets\":[{\"name\":\"Garfield\",\"type\":\"Dog\"},{\"name\":\"Fido\",\"type\":\"Dog\"}]},{\"name\":\"Jennifer\",\"gender\":\"Female\",\"age\":18,\"pets\":[{\"name\":\"Garfield\",\"type\":\"Dog\"}]},{\"name\":\"Steve\",\"gender\":\"Male\",\"age\":45,\"pets\":null},{\"name\":\"Fred\",\"gender\":\"Male\",\"age\":40,\"pets\":[{\"name\":\"Tom\",\"type\":\"Dog\"},{\"name\":\"Max\",\"type\":\"Dog\"},{\"name\":\"Sam\",\"type\":\"Dog\"},{\"name\":\"Jim\",\"type\":\"Dog\"}]},{\"name\":\"Samantha\",\"gender\":\"Female\",\"age\":40,\"pets\":[{\"name\":\"Tabby\",\"type\":\"Dog\"}]},{\"name\":\"Alice\",\"gender\":\"Female\",\"age\":64,\"pets\":[{\"name\":\"Simba\",\"type\":\"Dog\"},{\"name\":\"Nemo\",\"type\":\"Fish\"}]}]";
            var owners = JsonConvert.DeserializeObject<IEnumerable<Owner>>(sample);

            var sortEngine = new SortEngine();
            var genderList = sortEngine.Sort(owners);

            Assert.Equal(2, genderList.Count);
            Assert.Equal(0, genderList[Gender.Male].Count());
            Assert.Equal(0, genderList[Gender.Female].Count());
        }

        [Fact]
        public void TestNoFemaleOwners()
        {
            var sample = "[{\"name\":\"Bob\",\"gender\":\"Male\",\"age\":23,\"pets\":[{\"name\":\"Garfield\",\"type\":\"Dog\"},{\"name\":\"Fido\",\"type\":\"Dog\"}]},{\"name\":\"Jennifer\",\"gender\":\"Male\",\"age\":18,\"pets\":[{\"name\":\"Garfield\",\"type\":\"Dog\"}]},{\"name\":\"Steve\",\"gender\":\"Male\",\"age\":45,\"pets\":null},{\"name\":\"Fred\",\"gender\":\"Male\",\"age\":40,\"pets\":[{\"name\":\"Tom\",\"type\":\"Dog\"},{\"name\":\"Max\",\"type\":\"Dog\"},{\"name\":\"Sam\",\"type\":\"Dog\"},{\"name\":\"Jim\",\"type\":\"Dog\"}]},{\"name\":\"Samantha\",\"gender\":\"Male\",\"age\":40,\"pets\":[{\"name\":\"Tabby\",\"type\":\"Dog\"}]},{\"name\":\"Alice\",\"gender\":\"Male\",\"age\":64,\"pets\":[{\"name\":\"Simba\",\"type\":\"Dog\"},{\"name\":\"Nemo\",\"type\":\"Fish\"}]}]";
            var owners = JsonConvert.DeserializeObject<IEnumerable<Owner>>(sample);
            var expectedCatsOfMale = new string[] { "Garfield", "Garfield", "Jim", "Max", "Simba", "Tabby", "Tom" };

            var sortEngine = new SortEngine();
            var genderList = sortEngine.Sort(owners);

            Assert.Equal(1, genderList.Count);

            var catsOfMale = genderList[Gender.Male]?.ToList();
            for (var i = 0; i < catsOfMale.Count; i++)
            {
                Assert.Equal(expectedCatsOfMale[i], catsOfMale[i]);
            }
        }

        [Fact]
        public void TestNoOwners()
        {
            var owners = new List<Owner>();
            var sortEngine = new SortEngine();

            var ex = Assert.Throws<ArgumentException>(() => sortEngine.Sort(owners));
            Assert.Contains(CommonResources.NothingToSort, ex.Message);
        }

    }
}
