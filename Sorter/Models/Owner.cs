﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;

namespace Sorter.Models
{
    /// <summary>
    /// Owner class that gets deserialized
    /// </summary>
    public class Owner
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("gender")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Gender Gender { get; set; }

        [JsonProperty("age")]
        public int age { get; set; }

        [JsonProperty("pets")]
        public IEnumerable<Pet> Pets { get; set; }
    }
}
