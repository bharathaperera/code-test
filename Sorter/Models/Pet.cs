﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;

namespace Sorter.Models
{
    public class Pet
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        
        [JsonProperty("type")]  
        [JsonConverter(typeof(StringEnumConverter))]
        public PetType Type { get; set; }
    }
}
