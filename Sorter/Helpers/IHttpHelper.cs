﻿namespace Sorter.Helpers
{
    public interface IHttpHelper
    {
        /// <summary>
        /// Get deserialized T using the given service endpoint
        /// </summary>
        /// <param name="serviceEndPoint"></param>
        /// <returns>T</returns>
        T Get<T>(string serviceEndPoint);
    }
}
