﻿using Newtonsoft.Json;
using System;
using System.Net.Http;

namespace Sorter.Helpers
{
    public class HttpHelper : IHttpHelper
    {
        /// <summary>
        /// Get deserialized T using the given service endpoint
        /// </summary>
        /// <param name="serviceEndPoint"></param>
        /// <returns>T</returns>
        public T Get<T>(string serviceEndPoint)
        {
            using (var httpClient = new HttpClient())
            {
                var httpResponse = httpClient.GetAsync(serviceEndPoint).Result;
                if (httpResponse.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    throw new Exception($"{CommonResources.ServerError}: {httpResponse.StatusCode} - {httpResponse.Content.ReadAsStringAsync().Result}");
                }

                return JsonConvert.DeserializeObject<T>(httpResponse.Content.ReadAsStringAsync().Result);
            }
        }
    }
}

