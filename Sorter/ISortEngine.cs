﻿using Sorter.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sorter
{
    public interface ISortEngine
    {
        /// <summary>
        /// Groups the owners by gender and sorts the cat names by alphabetical order
        /// </summary>
        /// <param name="owners">List of <see cref="Owner"/> objects</param>
        /// <returns><see cref="Pet"/>A dictionary </returns>
        Dictionary<Gender, IEnumerable<string>> Sort(IEnumerable<Owner> owners);        
    }
}
