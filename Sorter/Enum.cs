﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sorter
{
    public enum Gender
    {
        Male = 0,
        Female = 1
    }

    public enum PetType
    {
        Other = 0,
        Cat,
        Dog,
        Fish
    }
}
