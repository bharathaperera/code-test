﻿using Sorter.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sorter
{
    public class SortEngine : ISortEngine
    {
        /// <summary>
        /// Groups the owners by gender and sorts the cat names by alphabetical order
        /// </summary>
        /// <param name="owners">List of <see cref="Owner"/> objects</param>
        /// <returns><see cref="Pet"/>A dictionary </returns>
        public Dictionary<Gender, IEnumerable<string>> Sort(IEnumerable<Owner> owners)
        {
            if (owners == null || !owners.Any())
            {
                throw new ArgumentException(CommonResources.NothingToSort, nameof(owners));
            }

            return owners.GroupBy(owner => owner.Gender)
                            .ToDictionary(g => g.Key,
                                            g => g.Where(owner => owner.Pets != null)
                                                .SelectMany(owner => owner.Pets.Where(pet => pet.Type == PetType.Cat))
                                                .OrderBy(cat => cat.Name)
                                                .Select(cat => cat.Name));
        }

    }
}
