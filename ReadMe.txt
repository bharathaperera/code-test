Tried to provide a simple, but reliable and a scalable solution.
	- Used C# core to program
	- Main project is a console app that utilizes the sorter to group and sort cat names
	- Tried to seperate business logic from webservice access
	- Used DI for lose coupling
	- Used Microsoft.Extensions.Logging for logging