﻿using Sorter.Helpers;
using Sorter.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using StructureMap;
using System;
using System.Collections.Generic;
using Sorter;
using Newtonsoft.Json;

namespace Main
{
    class Program
    {
        private static IServiceProvider ServiceProvider { get; set; }
        private static ILogger Logger { get; set; }
        private const string serviceEndPoint = @"http://agl-developer-test.azurewebsites.net/people.json";

        static void Main(string[] args)
        {
            SetupDi();
            SetupLogger();
            Logger.LogInformation("Starting application");

            try
            {
                var httpHelper = ServiceProvider.GetService<IHttpHelper>();
                var owners = httpHelper.Get<IEnumerable<Owner>>(serviceEndPoint);
                
                var sortEngine = ServiceProvider.GetService<ISortEngine>();
                var sortedOwnerList = sortEngine.Sort(owners);

                string stringContent = JsonConvert.SerializeObject(sortedOwnerList);
                Logger.LogInformation(stringContent);
            }
            catch (Exception ex)
            {
                Logger.LogError(1000, ex, ex.Message);
            }
            Logger.LogInformation("Press any key to exit...");
            Console.ReadLine();
        }
        
        // Setup DI container by using Structuremap API 
        private static void SetupDi()
        {
            //setup DI, add logging to the service collection
            var services = new ServiceCollection().AddLogging();

            //Add StructureMap
            var container = new Container();
            container.Configure(config =>
            {   
                config.Scan( c =>
                {
                    c.AssemblyContainingType(typeof(Owner));
                    c.WithDefaultConventions();
                });
                // populate the container using the service collection
                config.Populate(services);
            });

            ServiceProvider = container.GetInstance<IServiceProvider>();
        }

        // Setup the .Net core logging infrastructure
        private static void SetupLogger()
        {
            //configure console logging
            ServiceProvider
                .GetService<ILoggerFactory>()
                .AddConsole(LogLevel.Information);

            Logger = ServiceProvider
                .GetService<ILoggerFactory>()
                .CreateLogger<Program>();
        }
    }
}